# aws-marketplace

The artifacts required for submission to the AWS Marketplace, which currently includes building an AMI and a CloudFormation template to consume it

# workflow

![./README.svg](./README.svg)

# How Do I Deploy This Thing For My Own `DeployChannel`?

1. Ensure your bucket is publicly readable

   The provisioner needs to be able to `curl` _at least_ 3 things:
   * `stack.yaml` (well, that's AWS, but same-same)
   * `requirements.yml`
   * `helmfile.yaml`
 
1. Ensure it has [cfn-pivot](https://git.openraven.io/open/ansible-roles/cfn-pivot) in it

   From within the OpenRaven slack,
   
   `/cfn-pivot run publish-it my-awesome-bucket`
   
   will deploy **master** to your bucket, assuming the [gitlab-runner](https://console.aws.amazon.com/iam/home?region=us-west-2#/roles/gitlab-runner) has permission to write into your bucket; if you want to deploy a branch to your bucket, then [triggering CI manually](https://git.openraven.io/open/ansible-roles/cfn-pivot/pipelines/new) is the way to do that (although the same "able to write to your bucket" applies)
1. Ensure it has [helmfiles](https://git.openraven.io/open/helm-charts/helmfiles) in it

    From within the OpenRaven slack,
    
    `/helmfiles run publish-em my-awesome-bucket`
    
    will deploy **master** to your bucket, with all of the same caveats as the item right above
1. Ensure it has [./cloudformation/stack.yaml](./cloudformation/stack.yaml) in it

   Same same O.R. slack,
   
   `/aws-marketplace run publish-em my-awesome-bucket`
   
   same same **master**, caveats, etc
1. Fire up a CloudFormation stack

    That comes in two flavors:
    * I want to try out custom provisioning and/or helm-charts
    * I want to modify **this** repo's behavior, too
 
    For the first flow, use [the production CFN URL](https://console.aws.amazon.com/cloudformation/home#/stacks/quickcreate?templateURL=https://openraven-deploy.s3.amazonaws.com/stack.yaml&stackName=orvn-XXXX&param_OpenRavenGroupId=XXX) but **ensuring** it has your `&param_DeployChannel=my-awesome-bucket-here` in the URL (or modying the input param on the resulting AWS console page). Also, _be sure_ to replace those `XXX` with your Okta GroupId. Using the URL that the [production website](https://www.openraven.com/install) generates is fine, just be cognizant of the `DeployChannel`
    
    For the second flow, replace the `templateURL=` in there with a URL points to `https://my-awesome-bucket-here.s3.amazonaws.com/stack.yaml` (or whatever you named `cloudformation/stack.yaml` when it was copied to your bucket)

<!--
    <div id="cfn-url">https://console.aws.amazon.com/cloudformation/home#/stacks/quickcreate?templateURL=https://openraven-deploy.s3.amazonaws.com/stack.yaml&amp;stackName=orvn-XXXX&amp;param_OpenRavenGroupId=XXX</div>
    <label>Group Id: <input type="text" name="groupId" /></label><br/>
    <label>Your Bucket: <input type="text" name="deployChannel" /></label><br/>
    <button id="buildCfn">Build My Custom URL</button>
    <script>
    document.getElementById('buildCfn').addEventListener('click', () => {
    });
    </script>
/-->
