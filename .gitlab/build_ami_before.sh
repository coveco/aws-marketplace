#! /usr/bin/env bash
set -euo pipefail
if [[ -n "${TRACE:-}" ]]; then
    set -x
fi

apk --update-cache add --verbose python3 py3-pip

pip install --upgrade setuptools wheel
pip install --upgrade pip
# we use awscli in ami/manifest_to_stack_mapping.py to extract the SnapshotId
# for the resulting AMIs since packer does not offer them
pip install awscli

if [[ -n "${CI_SERVER_HOST:-}" ]]; then
    git config user.email "gitlab-runner@openraven.com"
    git config user.name "GitLab User"
    # put us back on a ref so the commit will do the right thing
    git checkout $CI_BUILD_REF_NAME
    git remote set-url --push origin "https://gitlab-runner:${GLR_PAT}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git"
fi
