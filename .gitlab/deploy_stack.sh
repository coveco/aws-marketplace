#!/usr/bin/env bash
set -euo pipefail
TRACE="${TRACE:-}"
if [[ -n "$TRACE" ]]; then
  set -x
fi

if ! command -v cfn-flip &>/dev/null; then
  pip3 install cfn-flip==1.2.3
fi

CHAT_INPUT="${CHAT_INPUT:-}"
if [[ -n "$CHAT_INPUT" ]]; then
  export HELM_S3_BUCKET="$CHAT_INPUT"
fi

chat_start
cd cloudformation
for f in *.yaml; do
  if [[ "$f" == *.iam.* ]]; then
    continue
  fi
  cfn-lint --info "$f"
done
# we need to distill down the main stack since with whitespace and comments
# it is bumping up against the CF TemplateBody limit (used by cfn-pivot)
for i in stack.yaml saas.yaml; do
    bn=$(basename $i .yaml)
    sed -i"" -e '/^Description:/s/$'"/ (${CI_COMMIT_SHORT_SHA})/" $i
    cfn-flip --json < $i | jq -c . > ${bn}.template
    cfn-flip --yaml --long         < ${bn}.template > $i
done
# Avoid doing processing on the filenames
# shellcheck disable=SC2035
../.gitlab/generate_manifest.py *.json *.template *.yaml
for f in *.json *.template *.yaml; do
    aws s3 cp "${f}" "s3://${HELM_S3_BUCKET}/${f}"
done
chat_stop
