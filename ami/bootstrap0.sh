#!/usr/bin/env bash
set -euo pipefail
DEBUG="${DEBUG:-}"
[[ -n "${DEBUG}" ]] && set -x

pypy_tar_url='https://downloads.python.org/pypy/pypy3.6-v7.3.2-linux64.tar.bz2'
# https://pypy.org/download.html#checksums
pypy_tar_sha256=d7a91f179076aaa28115ffc0a81e46c6a787785b2bc995c926fe3b02f0e9ad83

pre_pull_pypy() {
    local url="$1"
    local sha256="$2"
    output_fn=/root/$(basename "$url")
    if [[ ! -e        "$output_fn" ]]; then
        curl -fsSLo   "$output_fn" "$url"
        echo "$sha256  $output_fn" | sha256sum -c -
    fi
    mkdir -p /opt/bin
    tar -xjf "$output_fn" -C /opt/bin
    rm "$output_fn"
}

normalize_pypy() {
    local pypy_dir="$1"
    local pypy_bin="$2"
    local p
    ln -sf "$pypy_bin" "$pypy_dir"/bin/python
    "$pypy_dir"/bin/python -m ensurepip --upgrade --verbose
    for p in pip3 pip; do
      p="$pypy_dir"/bin/${p}
      [[ -x "$p" ]] || continue
      "$p" install --upgrade setuptools wheel
      "$p" install --upgrade pip
    done
}

pre_pull_pypy "$pypy_tar_url"   "$pypy_tar_sha256"

mv -iv /opt/bin/pypy3.*linux* /opt/bin/pypy
normalize_pypy /opt/bin/pypy pypy3

# make pypy3.6 the default python and pip binaries
for f in pip3 python; do
  cat >/opt/bin/${f} <<'SH'
#! /bin/bash
BINDIR="/opt/bin"
export LD_LIBRARY_PATH=$BINDIR/pypy/lib:$LD_LIBRARY_PATH
exec $BINDIR/pypy/bin/$(basename $0) "$@"
SH
  chmod 0755 /opt/bin/${f}
  /opt/bin/${f} --version
done

# this is a concession to kubespray
touch /opt/bin/.bootstrapped
